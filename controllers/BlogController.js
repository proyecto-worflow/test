const BlogModel = require ('../models/BlogModel.js');
const getAllBlogs = async (req, res)=> {
    try{
        const blogs = await BlogModel.findAll()
        res.json(blogs)
    }catch(error){
        res.json( {message: error.message} )
    }
}
module.exports = { getAllBlogs}

const getBlog = async (req, res)=> {
    try{
        const blog = await BlogModel.findAll({
            where:{
                id:req.params.id
            }
        })
        res.json(blog[0])
    }catch(error){
        res.json( {message: error.message} )
    }
}
module.exports = { getBlog}

const createBlog = async (req, res)=> {
    try{
        await BlogModel.create(req.body)
        res.json({
            "message":"Registro creado correctamente"
        })
    } catch (error){
        res.json( {message: error.message} )
    }
}
module.exports = {createBlog}

const updateBlog = async (req, res)=> {
    try{
        await BlogModel.update(req.body, {
            where: {id:req.params.id}
        })
        res.json({
            "message":"se actualizo correctamente"
        })
    }catch(error){
        res.json( {message: error.message} )
    }
}
module.exports = {updateBlog}
  
const deleteBlog = async (req, res)=> {
    try{
        await BlogModel.destroy({
            where: {id: req.params.id }
        })
        res.json({
            "message":"registro eliminado"
        })
    } catch (error){
        res.json( {message: error.message} )
    }
}
module.exports = {deleteBlog}