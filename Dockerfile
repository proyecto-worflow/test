FROM node:slim
WORKDIR /
COPY . /
RUN npm install
#RUN yarn build
#FROM bitnami/nginx:latest
#COPY --from=builder /
EXPOSE 8000
CMD node index.js