const express = require('express')
const app = express()
const port = 8000
const db = require("./database/db.js"); 
const blogRoutes = require("./routes/routes.js");


app.use( cors() )
app.use(express.json())
app.use('/blogs', blogRoutes)

const conexiondb = async (req, res)=> {
  try {
      await db.authenticate()
      console.log("Conexion con la db correcta")
  }catch (error){
      console.log(`error de conexion: ${error}`)
  }
}


app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})